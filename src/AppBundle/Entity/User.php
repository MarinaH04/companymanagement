<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;


/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserCompany", mappedBy="users")
     */
    private $usersCompany;



    public function __construct()
    {
        parent::__construct();
        $this->usersCompany = new ArrayCollection();
    }


    /**
     * @return ArrayCollection
     */
    public function getUsersCompany(): ArrayCollection
    {
        return $this->usersCompany;
    }

    /**
     * @param ArrayCollection $usersCompany
     */
    public function setUsersCompany(ArrayCollection $usersCompany): void
    {
        $this->usersCompany = $usersCompany;
    }


}