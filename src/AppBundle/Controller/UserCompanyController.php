<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Entity\UserCompany;
use AppBundle\Form\Type\UserCompanyType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserCompanyController extends Controller
{
    /**
     * @Route("/company/insert/{id}/{username}", name="show_company")
     */
    public function updateUCAction($id, $username, Request $request){
        $company = $this->getDoctrine()->getRepository('AppBundle:Company')->find($id);
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($username);

        $userCompany = new UserCompany();
        $userCompany->setCompany($company);
        $userCompany->setUsers($user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($userCompany);
        $em->flush();
        return $this->render('test/index.html.twig', array(
            'company' => $company, 'user'=>$user));
    }

    /**
     * @Route("/formusercompany",  name="usercompanies_index", methods={"GET"})
     */
    public function createAction()
    {

        $form = $this->createForm(UserCompanyType::class, new UserCompany());

        return $this->render('usercompany/index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/formusercompany", methods={"POST"})
     */
    public function create2Action(Request $request)
    {

        $form = $this->createForm(UserCompanyType::class, new UserCompany());
        $data = $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data->getData());
        $em->flush();
        return $this->redirect($this->generateUrl('usercompanies_index'));
    }


}