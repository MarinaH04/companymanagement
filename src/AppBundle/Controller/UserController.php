<?php


namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\Type\CompanyType;
use AppBundle\Form\Type\UserType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class UserController extends Controller
{

    /**
     * @Route("/users", name="users_index")
     */
    public function indexAction(Request $request)
    {
        $user = new User();
        $user->setUsername("Cristina05");
        $user->setUsernameCanonical("Cristina05");
        $user->setEmail("cristina@yahoo.com");
        $user->setEmailCanonical("cristina@yahoo.com");
        $user->setEnabled(1);
        $user->setPassword("1234");

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->render('user/index.html.twig');
    }

    /**
     * @Route("/formuser",  name="users_index", methods={"GET"})
     */
    public function createUserAction()
    {
        $form = $this->createForm(UserType::class, new User());

        return $this->render('user/index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/formuser", methods={"POST"})
     */
    public function createUser2Action(Request $request)
    {
        $form = $this->createForm(UserType::class, new User());
        $data = $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data->getData());
        $em->flush();
        return $this->redirect($this->generateUrl('users_index'));
    }


}