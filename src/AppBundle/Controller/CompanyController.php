<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Form\Type\CompanyType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends Controller
{
    /**
     * @Route("/companies", name="companies_index")
     */
    public function indexAction(Request $request){
        $company = new Company();
        $company->setName("Continental");


        $em = $this->getDoctrine()->getManager();
        $em->persist($company);
        $em->flush();

        $em = $this->getDoctrine()->getManager();
        $companies = $em->getRepository(Company::class)->findAll();
        return $this->render('company/index.html.twig', array(
            'companies' => $companies
        ));
    }

    /**
     * @Route("/formcompany",  name="companies_index", methods={"GET"})
     */
    public function createAction()
    {
        $form = $this->createForm(CompanyType::class, new Company());

        return $this->render('company/company.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/formcompany", methods={"POST"})
     */
    public function create2Action(Request $request)
    {
        $form = $this->createForm(CompanyType::class, new Company());
        $data = $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data->getData());
        $em->flush();
        return $this->redirect($this->generateUrl('companies_index'));
    }
}