<?php


namespace AppBundle\Form\Type;


use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('users', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'email'])
            ->add('companies', EntityType::class, [
                'class' => Company::class,
                'choice_label' => 'name']);

    }

}